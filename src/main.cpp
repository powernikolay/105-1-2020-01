#include <iostam>

int main()
{
  std::cout<<"=========="<<std::end;
  int a = 1, b = 4;
  auto result = a + b - a%b;
  std::cout<<"Result: " << result << std::end;

  std::cout<<"=========="<<std::end;
  return 0;
}